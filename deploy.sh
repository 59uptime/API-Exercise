#!/bin/bash
#Variables
docker_repo="containersolutions/";
docker_relative_path="./docker/";

# Sources functions from functions.sh.
. functions.sh


### Cleanup environment these commands are used to cleanup the environment.
delete_service titanic-sql 
delete_pod titanic-sql
delete_service titanic-api 
delete_pod titanic-api
delete_secret secret-mysql


#create secret
set_secret

#Deploy SQL
set_docker_to_minikube;
build_image titanic-sql :0.1 sql/.;
create_k8s_deployment titanic-sql :0.1 titanic-sql;
set_k8s_env titanic-sql MYSQL_ROOT_PASSWORD="my-#s#e#c#ret-pw";
expose_port titanic-sql 3306;


#deploy API
set_docker_to_minikube;
export_secret ./docker/api/src/secret;
build_image titanic-api :0.1 api/.;
create_k8s_deployment titanic-api :0.1 titanic-api;
set_k8s_env titanic-api;
expose_node_port titanic-api 8000;
expose_url titanic-api



