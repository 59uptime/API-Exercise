#!/bin/bash

# Command: set_docker_to_minikube
### Function source Minikube docker environment
# EXAMPLE: set_docker_to_minikube;

# Command: build_image
### Function has two parameteres and is used to build a docker image.
# Parameter 1 = Tag Version
# Parameter 2 = docker folder name with trailing slash (/)
# EXAMPLE build_image :0.1 sql/.;

# Command: create_k8s_deployment
### Function has two parameteres and is used to create a deployment/pod.
# Parameter 1 = Tag Version
# Parameter 2 = Docker Name
# EXAMPE create_k8s_deployment :01 titanic-sql;

# Command: set_k8s_env
### Function has one parametere with trailing environment variables
# Parameter 1 = Docker Name
# Add key value environment values
# EXAMPLE set_k8s_env titanic-sql MYSQL_ROOT_PASSWORD="my-#s#e#c#ret-pw";

# Command: expose_port
### Function has two parameteres and is used to expose pod port.
# Parameter 1 = Docker Name
# Parameter 2 = Port Number
# EXAMPLE: expose_port titanic-sql 3306;

# Command: expose_node_port 
### Function has two parameteres and is used to expose node port.
# Parameter 1 = Docker Name
# Parameter 2 = Port Number
# EXAMPLE: expose_node_port titanic-sql 3306;

# Command: expose_url
### Function used to expose URL in MiniKube
# Parameter 1 = Docker Name
# Example: expose_url

# Command: set_secret
### Function used to create Kubernetes secret
# Modify secret.yaml contents to change secret.
# Example: set_secret

# Command: export_secret
### Function used to export secret from Kubernetes into a file that can be used during docker deployment stage.
# Parameter 1 = secret file path name.


set_docker_to_minikube () {
    eval $(minikube docker-env)
}

build_image() {
    docker_name_parameter=$1
    docker_tag_parameter=$2
    docker_foldername_parameter=$3
    docker image build -t ${docker_repo}${docker_name_parameter}${docker_tag_parameter} ${docker_relative_path}${docker_foldername_parameter}
}

create_k8s_deployment () {
    docker_name_parameter=$1
    docker_tag_parameter=$2
    docker_name_parameter=$3
    kubectl create deployment --image ${docker_repo}${docker_name_parameter}${docker_tag_parameter} ${docker_name_parameter}
}

set_k8s_env () {
    docker_name_parameter=$1
    kubectl set env deployment/${docker_name_parameter} $2 $3 $4 $5 $6 $7 $8 $9
}

expose_port () {
    docker_name_parameter=$1
    port_number=$2
    kubectl expose deployment ${docker_name_parameter}  --port ${port_number} --target-port ${port_number}
}

expose_node_port () {
    docker_name_parameter=$1
    port_number=$2
    kubectl expose deployment ${docker_name_parameter} --type=NodePort --port ${port_number}
}

expose_url() {
    docker_name_parameter=$1
    minikube service ${docker_name_parameter} --url
}

set_secret () {
    kubectl create -f secret.yaml
}

export_secret () {
    export secret=$(kubectl get secret secret-mysql -o json | jq .data.password | sed -e 's/^"//' -e 's/"$//' | base64 -d ; echo)
    echo $secret > $1
    secret=""
}

delete_secret () {
    kubectl delete secret $1
}

delete_service () {
    kubectl delete service $1
}

delete_pod () {
    kubectl delete deployment/$1
}
