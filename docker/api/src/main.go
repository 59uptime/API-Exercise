package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type Post struct {
	Uuid                    string  `json:"uuid"`
	Survived                bool    `json:"survived"`
	PassengerClass          int     `json:"passengerClass"`
	Name                    string  `json:"name"`
	Sex                     string  `json:"sex"`
	Age                     int     `json:"age"`
	SiblingsOrSpousesAboard int     `json:"siblingsOrSpousesAboard"`
	ParentsOrChildrenAboard int     `json:"parentsOrChildrenAboard"`
	Fare                    float32 `json:"fare"`
}

var db *sql.DB
var err error

// Database connection and Routes to end points created

func main() {
	db, err = sql.Open("mysql", "root:CHANGEME@tcp(titanic-sql:3306)/titanic")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	router := mux.NewRouter()
	router.HandleFunc("/people", getPosts).Methods("GET")
	router.HandleFunc("/people", createPost).Methods("POST")
	router.HandleFunc("/people/{uuid}", getPost).Methods("GET")
	router.HandleFunc("/people/{uuid}", updatePost).Methods("PUT")
	router.HandleFunc("/people/{uuid}", deletePost).Methods("DELETE")
	http.ListenAndServe(":8000", router)
}

// Get posts grabs all the people from the titanic database

func getPosts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var posts []Post
	result, err := db.Query("Select * FROM mytable")
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	for result.Next() {
		var post Post
		err := result.Scan(&post.Uuid, &post.Survived, &post.PassengerClass, &post.Name, &post.Sex, &post.Age, &post.SiblingsOrSpousesAboard, &post.ParentsOrChildrenAboard, &post.Fare)
		if err != nil {
			log.Fatal("Unable to parse row:", err)
		}
		posts = append(posts, post)
	}
	json.NewEncoder(w).Encode(posts)
}

// This portion allows for new people creation as per the project requirement.

func createPost(w http.ResponseWriter, r *http.Request) {
	stmt, err := db.Prepare("INSERT INTO mytable(uuid, survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare) VALUES(uuid(), ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	boolVal := make(map[string]bool)
	keyVal := make(map[string]string)
	intVal := make(map[string]int)
	floatVal := make(map[string]float32)
	json.Unmarshal(body, &boolVal)
	json.Unmarshal(body, &keyVal)
	json.Unmarshal(body, &intVal)
	json.Unmarshal(body, &floatVal)
	survived := boolVal["survived"]
	passengerClass := intVal["passengerClass"]
	name := keyVal["name"]
	sex := keyVal["sex"]
	age := intVal["age"]
	siblingsOrSpousesAboard := intVal["siblingsOrSpousesAboard"]
	parentsOrChildrenAboard := intVal["parentsOrChildrenAboard"]
	fare := floatVal["fare"]
	_, err = stmt.Exec(survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare)
	fmt.Println(stmt)
	if err != nil {
		log.Fatal("Unable to parse row:", err)
	}
	w.Header().Set("Content-Type", "application/json")
	result, err := db.Query("SELECT uuid, survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare FROM mytable  where name  = ?", name)
	if err != nil {
		log.Fatal("Unable to parse row:", err)

	}
	defer result.Close()
	var post Post
	for result.Next() {
		err := result.Scan(&post.Uuid, &post.Survived, &post.PassengerClass, &post.Name, &post.Sex, &post.Age, &post.SiblingsOrSpousesAboard, &post.ParentsOrChildrenAboard, &post.Fare)
		if err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(post)
}

// This portion of the code gets a signle person via uuid specified at the end of the route.

func getPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	result, err := db.Query("SELECT uuid, survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare FROM mytable  where uuid  = ?", params["uuid"])
	if err != nil {
		log.Fatal("Unable to parse row:", err)

	}
	defer result.Close()
	var post Post
	for result.Next() {
		err := result.Scan(&post.Uuid, &post.Survived, &post.PassengerClass, &post.Name, &post.Sex, &post.Age, &post.SiblingsOrSpousesAboard, &post.ParentsOrChildrenAboard, &post.Fare)
		if err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(post)
}

// This portion of the code allows us to make changes to an exsisting person.

func updatePost(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	stmt, err := db.Prepare("UPDATE mytable SET survived = ?, passengerClass = ?, name = ?, sex = ?, age = ?, siblingsOrSpousesAboard = ?, parentsOrChildrenAboard = ?, fare = ? WHERE uuid = ?")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	boolVal := make(map[string]bool)
	keyVal := make(map[string]string)
	intVal := make(map[string]int)
	floatVal := make(map[string]uint8)
	json.Unmarshal(body, &boolVal)
	json.Unmarshal(body, &keyVal)
	json.Unmarshal(body, &intVal)
	json.Unmarshal(body, &floatVal)
	survived := boolVal["survived"]
	passengerClass := intVal["passengerClass"]
	name := keyVal["name"]
	sex := keyVal["sex"]
	age := intVal["age"]
	siblingsOrSpousesAboard := intVal["siblingsOrSpousesAboard"]
	parentsOrChildrenAboard := intVal["parentsOrChildrenAboard"]
	fare := floatVal["fare"]
	_, err = stmt.Exec(survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare, params["uuid"])
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "Post with uuid = %s was updated", params["uuid"])
}

// This portion of the code allows us to delete a person from the database.

func deletePost(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	stmt, err := db.Prepare("DELETE FROM mytable WHERE uuid = ?")
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(params["uuid"])
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "Post with uuid = %s was deleted", params["uuid"])
}
