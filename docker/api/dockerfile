

############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder
# Install git.
# Git is required for fetching the dependencies.
ENV APP_USER app
ENV APP_HOME /go/src/titanic
RUN addgroup -S $APP_USER && adduser -S $APP_USER  -g $APP_USER 
RUN mkdir -p $APP_HOME && chown -R $APP_USER:$APP_USER $APP_HOME
WORKDIR $APP_HOME
USER $APP_USER
COPY src/ .
RUN export secret=$(cat /go/src/titanic/secret) && sed -i "s/CHANGEME/${secret}/g" ./main.go
RUN go mod download
RUN go mod verify
RUN go build -o titanic
RUN rm /go/src/titanic/secret
RUN rm /go/src/titanic/main.go

############################
# STEP 2 build a small image
############################
FROM alpine 
# Copy our static executable.
ENV APP_USER app
ENV APP_HOME /go/bin/
RUN addgroup -S $APP_USER && adduser -S $APP_USER  -g $APP_USER
RUN mkdir -p /go/bin/ && chown -R $APP_USER $APP_HOME
USER $APP_USER 
COPY --from=builder /go/src/titanic/titanic /go/bin/titanic
# Run the hello binary.
ENTRYPOINT ["/go/bin/titanic"]